module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
    'es6': true,
    'node': true
  },
  'extends': 'eslint:recommended',
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
    'app': 'readonly',
    'urlB64ToUint8Array': 'readonly',
    'paperIndexedDB': 'readonly',
    'axios': 'readonly',
    'importScripts': 'readonly'
  },
  'parserOptions': {
    'ecmaVersion': 2015,
    'sourceType': 'module'
  },
  'rules': {
    'semi': [2, 'always', { 'omitLastInOneLineBlock': true}],
    'no-trailing-spaces': 'error',
    'quotes': ['error', 'single'],
    'comma-dangle': ['error', 'never']
  }
};
