/*
MIT License
Copyright (c) 2020 GeunHyeok LEE
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
// @license MIT
// @version 0.1.0
// @author Geunhyeok LEE

class VBrick {
  constructor () {
    this._bricks = [];
  }

  is (value) {
    this._bricks.push(v => v === value);
    return this;
  }

  isNot (value) {
    this._bricks.push(v => v !== value);
    return this;
  }

  or (...args) {
    this._bricks.push(v => {
      for (let i = 0; i < args.length; i++) {
        if (v === args[i]) {
          return true;
        }
      }
      return false;
    });
    return this;
  }

  type (type) {
    this._bricks.push(v => v.__proto__ === type.prototype);
    return this;
  }

  min (n) {
    this._bricks.push(v => n <= v);
    return this;
  }

  max (n) {
    this._bricks.push(v => n >= v);
    return this;
  }

  regexp (exp) {
    this._bricks.push(v => exp.test(v.toString()));
    return this;
  }

  func (f) {
    if (typeof f !== 'function') {
      throw new Error('argument must be function');
    }

    this._bricks.push(v => {
      if (Array.isArray(v)) {
        for (let i = 0; i < v.length; i++) {
          if (!f(v[i])) {
            return false;
          }
        }
        return true;
      } else {
        return f(v);
      }
    });

    return this;
  }

  build () {
    return {
      validate: value => {
        for (let i = 0; i < this._bricks.length; i++) {
          if (!this._bricks[i](value)) {
            return false;
          }
        }
        return true;
      }
    };
  }
}

export default VBrick;
