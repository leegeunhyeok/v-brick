import VBrick from '../index';

if (window) {
  window.VBrick = VBrick;
}

export default VBrick;